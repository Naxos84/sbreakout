package de.hpi.javaide.breakout.screens;

import de.hpi.javaide.breakout.interfaces.Displayable;
import de.hpi.javaide.breakout.interfaces.Initializable;
import de.hpi.javaide.breakout.interfaces.Updateable;

/**
 * Provides a common abstract class for all Screen classes, so that we can
 * easily switch between the states of the Screen.
 *
 * @author Ralf Teusner and Tom Staubitz
 *
 */
public abstract class Screen implements Initializable, Displayable, Updateable {
	public static final String START = "start";
	public static final String GAME = "game";
	public static final String END = "end";

	protected Screen() {
	}

	public void handleKeyPressed(final int key) {
	}

	public void handleMouseDragged() {
	}

	public abstract void handleMouseClick(int mouseX, int mouseY);
}
