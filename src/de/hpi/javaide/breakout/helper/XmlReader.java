package de.hpi.javaide.breakout.helper;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.newdawn.slick.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlReader {

	private Document doc;

	public XmlReader(final String path) {
		final File xmlFile = new File(path);
		final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try {
			final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlFile);
		} catch (SAXException | IOException e) {
			Log.error("Error while parsing xml file: " + path, e);
			doc = null;
		} catch (final ParserConfigurationException e) {
			Log.error("error creating Document for xml file: " + path, e);
			doc = null;
		}
	}

	public NodeList getElementByTag(final String tag) {
		return doc.getElementsByTagName(tag);
	}
}
