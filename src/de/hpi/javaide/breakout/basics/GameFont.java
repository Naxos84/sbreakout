package de.hpi.javaide.breakout.basics;

import java.awt.Font;

import org.newdawn.slick.TrueTypeFont;

/**
 * Creates some Fonts in different sizes and make them easily available. Adapter
 * to the PFont class as offered by Processing
 *
 * @author Ralf Teusner and Tom Staubitz
 *
 */
public class GameFont {

	private static Font font16 = new Font("Arial", Font.PLAIN, 16);
	private static Font font24 = new Font("Arial", Font.PLAIN, 24);
	private static Font font32 = new Font("Arial", Font.PLAIN, 32);

	private static TrueTypeFont f16;
	private static TrueTypeFont f24;
	private static TrueTypeFont f32;

	private GameFont() {
	}

	public static void init() {
		f16 = new TrueTypeFont(font16, false);
		f24 = new TrueTypeFont(font24, false);
		f32 = new TrueTypeFont(font32, false);
	}

	/**
	 *
	 * @return a default font with size 16
	 */
	public static TrueTypeFont getFont16() {
		return f16;
	}

	/**
	 *
	 * @return a default font with size 24
	 */
	public static TrueTypeFont getFont24() {
		return f24;
	}

	/**
	 *
	 * @return a default font with size 32
	 */
	public static TrueTypeFont getFont32() {
		return f32;
	}

	/**
	 * Returns the width of the given {@link String} and the used
	 *
	 * @param text
	 *            the text of which the width should be calculated
	 * @param font
	 *            the font of the text
	 * @return the calculated width of the text
	 */
	@SuppressWarnings("deprecation")
	public static float getTextWidth(final String text, final TrueTypeFont font) {
		// I need to use this method. because an adequate Processing solution
		// exists only for version 3+
		return font.getWidth(text);
	}

	/**
	 * Returns the height of the given {@link String} and the used
	 *
	 * @param text
	 *            the text of which the height should be calculated
	 * @param font
	 *            the font of the text
	 * @return the calculated height of the text
	 */
	@SuppressWarnings("deprecation")
	public static float getTextHeight(final String text, final TrueTypeFont font) {
		// I need to use this method. because an adequate Processing solution
		// exists only for version 3+
		return font.getHeight(text);
	}
}
