package de.hpi.javaide.breakout.starter;

import org.apache.log4j.PropertyConfigurator;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Main {

	private Main() {
	}

	/**
	 * the entry point of this application
	 *
	 * @param args
	 * @throws SlickException
	 */
	public static void main(final String[] args) throws SlickException {
		PropertyConfigurator.configureAndWatch("log4j.properties", 60 * 1000L);

		final org.newdawn.slick.AppGameContainer container = new AppGameContainer(new Game("SBreakout"));
		container.setShowFPS(true);
		container.setDisplayMode(GameConstants.SCREEN_X, GameConstants.SCREEN_Y, false);
		container.setVSync(true);
		container.start();
	}

}
