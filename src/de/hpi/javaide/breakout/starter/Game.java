package de.hpi.javaide.breakout.starter;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import de.hpi.javaide.breakout.basics.GameFont;
import de.hpi.javaide.breakout.interfaces.Pauseable;
import de.hpi.javaide.breakout.screens.Screen;
import de.hpi.javaide.breakout.screens.ScreenManager;

public class Game extends BasicGame {

	private static int score;

	public Game(final String title) {
		super(title);
	}

	// Setup the game
	@Override
	public void init(final GameContainer container) {
		GameFont.init();
		ScreenManager.setScreen(container, Screen.START);
	}

	// draw everything in the game
	@Override
	public void render(final GameContainer container, final Graphics graphics) {
		ScreenManager.getCurrentScreen().display(container, graphics);

	}

	// update everything in the game
	@Override
	public void update(final GameContainer container, final int delta) throws SlickException {
		ScreenManager.getCurrentScreen().update(container, delta);
	}

	@Override
	public void keyPressed(final int key, final char c) {
		final Screen screen = ScreenManager.getCurrentScreen();
		screen.handleKeyPressed(key);
		if (key == Input.KEY_ESCAPE) {
			if (screen instanceof Pauseable) {
				final Pauseable pauseable = (Pauseable) screen;
				if (pauseable.isPaused()) {
					pauseable.unpause();
				} else {
					pauseable.pause();
				}
			}
		} else if (key == Input.KEY_Q) {
			// pressing "Q" quits the game
		}
	}

	public static void addToScore(final int score) {
		Game.score += score;
	}

	public static int getScore() {
		return Game.score;
	}

}
