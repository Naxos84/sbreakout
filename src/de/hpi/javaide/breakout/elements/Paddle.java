package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.geom.Point2D;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import de.hpi.javaide.breakout.basics.Color;
import de.hpi.javaide.breakout.basics.Rectangular;
import de.hpi.javaide.breakout.interfaces.Initializable;
import de.hpi.javaide.breakout.interfaces.Updateable;

/**
 * Create the paddle
 *
 * @param game
 *            Game provide access to the Processing features
 */
public class Paddle extends Rectangular implements Updateable, Initializable {
	public Paddle() {
		super(new Point2D.Float(0, 0), new Dimension(0, 0));
		setColor(Color.GREY);
	}

	@Override
	public void init(final GameContainer container) {
		// TODO Auto-generated method stub
		super.update(new Point2D.Float(container.getWidth() / 2, container.getHeight() - 20), new Dimension(100, 20));
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		// game.rectMode(PApplet.CORNER);
		// game.noStroke();
		// game.fill(getRed(), getGreen(), getBlue());
		// game.rect(getXPosition(), getYPosition(), getWidth(), getHeight());
		graphics.setColor(org.newdawn.slick.Color.white);
		graphics.drawRect(getXPosition(), getYPosition(), getWidth(), getHeight());
	}

	@Override
	public void update(final GameContainer container, final int delta) {
		final Input input = container.getInput();

		super.update(new Point2D.Float(input.getMouseX() - getWidth() / 2, getYPosition()),
				new Dimension(getWidth(), getHeight()));
	}
}
