package de.hpi.javaide.breakout.elements;

import java.awt.geom.Point2D;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import de.hpi.javaide.breakout.basics.Circle;
import de.hpi.javaide.breakout.starter.GameConstants;

/**
 * Blueprint for a Ball
 *
 * @author Ralf Teusner and Tom Staubitz
 *
 */
public class Ball extends Circle {

	private Vector2f vector;

	public Ball(final Point2D.Float position) {
		super(position, GameConstants.BALL_RADIUS);
		vector = new Vector2f(1, 1);

	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		graphics.setColor(new Color(GameConstants.BALL_COLOR.getRed(), GameConstants.BALL_COLOR.getGreen(),
				GameConstants.BALL_COLOR.getBlue()));
		graphics.fillOval(position.x, position.y, getRadius(), getRadius());
		graphics.drawOval(position.x, position.y, getRadius(), getRadius());
		// game.ellipseMode(PApplet.CENTER);
	}

	/**
	 * moves the ball according to its vector
	 */
	public void move(final int delta) {
		position.x += vector.x * delta / 1000f;
		position.y += vector.y * delta / 1000f;
	}

	public void setVector(final Vector2f newVector) {
		vector = newVector;
	}

	public void bounceY() {
		vector.y *= -1;
	}

	public void bounceX() {
		vector.x *= -1;
	}

	public float getSpeed() {
		return GameConstants.BALL_SPEED;
	}

}
