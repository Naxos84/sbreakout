package de.hpi.javaide.breakout.elements.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

import de.hpi.javaide.breakout.basics.GameFont;
import de.hpi.javaide.breakout.basics.UIObject;

public class Info extends UIObject<String> {

	private String content;
	private final TrueTypeFont font = GameFont.getFont24();

	public Info(final String content) {
		this.content = content;
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		font.drawString(xPosition, yPosition, content, Color.white);
	}

	@Override
	public void update(final String input) {
		content = input;

	}

	/**
	 *
	 * @return the width of the text represented by this Info Box
	 */
	public float getFontWidth() {
		return GameFont.getTextWidth(content, font);
	}
}
