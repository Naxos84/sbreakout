package de.hpi.javaide.breakout.elements.ui;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.TrueTypeFont;

import de.hpi.javaide.breakout.basics.Color;
import de.hpi.javaide.breakout.basics.GameFont;
import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.helper.ResourceManager;
import de.hpi.javaide.breakout.starter.GameConstants;

public class Menu extends UIObject<List<String>> {

	private static final Color SELECTED = new Color(0, 255, 0);
	private static final Color UNSELECTED = new Color(255, 255, 255);

	private List<String> keys;
	private int menuSelection;
	private final TrueTypeFont font = GameFont.getFont16();

	public Menu() {
		keys = new ArrayList<>();
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		for (int i = 0; i < keys.size(); i++) {
			if (i == menuSelection) {
				graphics.setColor(
						new org.newdawn.slick.Color(SELECTED.getRed(), SELECTED.getGreen(), SELECTED.getBlue()));
			} else {
				graphics.setColor(
						new org.newdawn.slick.Color(UNSELECTED.getRed(), UNSELECTED.getGreen(), UNSELECTED.getBlue()));
			}
			final String text = ResourceManager.getString(keys.get(i));
			graphics.drawString(text, GameConstants.SCREEN_X / 2f - GameFont.getTextWidth(text, font),
					200 + i * GameFont.getTextHeight(text, font));
			graphics.setColor(
					new org.newdawn.slick.Color(UNSELECTED.getRed(), UNSELECTED.getGreen(), UNSELECTED.getBlue()));
		}
	}

	@Override
	public void update(final List<String> input) {
		keys = input;

	}

	/**
	 * handle a pressed key
	 *
	 * @param keyCode
	 *            the keyCode of the pressed key
	 */
	public void onKeyPress(final int keyCode) {
		switch (keyCode) {
		case Input.KEY_UP:
			decreaseMenuSelection();
			break;
		case Input.KEY_DOWN:
			increaseMenuSelection();
			break;
		default:
			break;
		}
	}

	/**
	 *
	 * @return the selection of this menu
	 */
	public int getMenuSelection() {
		return menuSelection;
	}

	private void increaseMenuSelection() {
		menuSelection++;
		if (menuSelection >= keys.size()) {
			menuSelection = 0;
		}
	}

	private void decreaseMenuSelection() {
		menuSelection--;
		if (menuSelection < 0) {
			menuSelection = keys.size() - 1;
		}
	}

}
