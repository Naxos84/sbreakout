package de.hpi.javaide.breakout.elements.ui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.TrueTypeFont;

import de.hpi.javaide.breakout.basics.Color;
import de.hpi.javaide.breakout.basics.GameFont;
import de.hpi.javaide.breakout.basics.UIObject;

public class Button extends UIObject<String> {

	private String text = "";
	private static final TrueTypeFont font = GameFont.getFont16();

	public void setText(final String text) {
		this.text = text;
	}

	public float getWidth() {
		return GameFont.getTextWidth(text, font);
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {

		font.drawString(xPosition, yPosition, text);
	}

	@Override
	public void update(final String input) {
		text = input;
	}

	/**
	 * sets the focus of this button. This is used for selecting them via the
	 * keyboard
	 *
	 * @param isFocussed
	 *            wether this button has focus or not
	 */
	public void setFocus(final boolean isFocussed) {
		if (isFocussed) {
			color.setColor(Color.GREEN);
		} else {
			color = getDefaultColor();
		}
	}

}
