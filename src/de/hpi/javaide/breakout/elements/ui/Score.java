package de.hpi.javaide.breakout.elements.ui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import de.hpi.javaide.breakout.basics.UIObject;
import de.hpi.javaide.breakout.starter.Game;

public class Score extends UIObject<String> {

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		graphics.drawString(String.valueOf(Game.getScore()), 10f, 30f);

	}

	@Override
	public void update(final String input) {
		// score is self updating see display()
	}
}
