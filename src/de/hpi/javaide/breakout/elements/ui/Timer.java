package de.hpi.javaide.breakout.elements.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import de.hpi.javaide.breakout.basics.UIObject;

public class Timer extends UIObject<String> {

	private final int seconds;

	public Timer() {
		seconds = 60;
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		graphics.setColor(Color.black);
		// game.textFont(GameFont.getFont16());
		graphics.drawString("Time left: " + seconds, container.getWidth() - 150f, container.getHeight() - 80f);
	}

	@Override
	public void update(final String input) {
		// timer is not implemented yet

	}
}
