package de.hpi.javaide.breakout.elements;

import java.awt.geom.Point2D;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

import de.hpi.javaide.breakout.interfaces.Displayable;
import de.hpi.javaide.breakout.interfaces.Measureable;
import de.hpi.javaide.breakout.starter.GameConstants;

public class BallDepot implements Displayable, Measureable {

	private static final Point2D.Float STARTPOSITION = new Point2D.Float(GameConstants.SCREEN_X / 2f,
			GameConstants.SCREEN_Y / 2f);

	private int numberOfBalls;

	public BallDepot() {
		this(GameConstants.BALLDEPOT_NUM_BALLS);
	}

	public BallDepot(final int numberOfBalls) {
		this.numberOfBalls = numberOfBalls;
	}

	@Override
	public float getXPosition() {
		return 10;
	}

	@Override
	public float getYPosition() {
		return 40;
	}

	@Override
	public int getWidth() {
		return 0;
	}

	@Override
	public int getHeight() {
		return 0;
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		graphics.setColor(Color.red);
		for (int i = 0; i < numberOfBalls; i++) {
			graphics.drawOval(getXPosition() + i * 10, getYPosition(), 5, 5);

		}
	}

	public boolean isEmpty() {
		return numberOfBalls <= 0;
	}

	/**
	 * creates a ball and removes one ball from the ball depot
	 *
	 * @return
	 */
	public Ball dispense() {
		if (numberOfBalls > 0) {
			final Ball ball = new Ball(STARTPOSITION);
			final Vector2f v = new Vector2f(1, 1);
			v.normalise();
			v.scale(ball.getSpeed());
			ball.setVector(v);
			numberOfBalls--;
			return ball;
		} else {
			return null;
		}
	}
}
