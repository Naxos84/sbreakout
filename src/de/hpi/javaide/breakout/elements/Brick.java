package de.hpi.javaide.breakout.elements;

import java.awt.Dimension;
import java.awt.geom.Point2D;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.Log;

import de.hpi.javaide.breakout.basics.Rectangular;
import de.hpi.javaide.breakout.interfaces.Displayable;
import de.hpi.javaide.breakout.interfaces.Initializable;
import de.hpi.javaide.breakout.starter.Game;

public class Brick extends Rectangular implements Displayable, Initializable {
	private static final int TOTAL_IMAGES = 3;

	private int hitPoints;

	private Image[] images;

	public Brick(final Point2D.Float position, final Dimension dimension) {
		this(position, dimension, TOTAL_IMAGES);
	}

	public Brick(final Point2D.Float position, final Dimension dimension,
			final int hitPoints) {
		super(position, dimension);
	}

	@Override
	public void init(final GameContainer game) {
		hitPoints = TOTAL_IMAGES;
		if (images == null) {
			images = new Image[3];
			try {
				images[0] = new Image("resources\\images\\Brick0.png");
				images[1] = new Image("resources\\images\\Brick1.png");
				images[2] = new Image("resources\\images\\Brick2.png");
			} catch (final SlickException e) {
				Log.error("Error when creating Images for Brick.", e);
			}
		}
	}

	@Override
	public void display(final GameContainer container, final Graphics graphics) {
		if (hasHitPoints()) {
			images[TOTAL_IMAGES - hitPoints].draw(position.x, position.y, dimension.width, dimension.height);
			// graphics.drawImage(images[TOTAL_IMAGES - hitPoints], position.x,
			// position.y);
			// game.image(images[TOTAL_IMAGES - hitPoints], position.x,
			// position.y, dimension.width, dimension.height);
		}
	}

	/**
	 * checks wether this brick has remaining hitPoints
	 *
	 * @return {@code true} if this Brick still has some hitPoints and
	 *         {@code false} otherwise
	 */
	public boolean hasHitPoints() {
		return hitPoints > 0;
	}

	/**
	 * handle that this brick was hit
	 */
	public void onHit() {
		hitPoints--;
		Game.addToScore(1);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName());
		sb.append(" P: [").append(getXPosition()).append(",").append(getYPosition()).append("]");
		sb.append(" - ");
		sb.append(" D: [").append(getWidth()).append(",").append(getHeight()).append("]");
		return sb.toString();
	}

}
